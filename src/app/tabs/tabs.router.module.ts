import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: 'tabs',
    component: TabsPage,
    children: [
      {
        path: 'dashboard',
        children: [{
          path: '',
          loadChildren: '../tabs/dashboard/dashboard.module#DashboardPageModule'
        }]
      },
      {
        path: 'referrals',
        children: [{
          path: '',
          loadChildren: '../tabs/referrals/referrals.module#ReferralsPageModule'
        }]
      },
      {
        path: 'profile',
        children: [{
          path: '',
          loadChildren: '../tabs/profile/profile.module#ProfilePageModule'
        }]
      },
      {
        path: 'division',
        children: [{
          path: '',
          loadChildren: '../tabs/division/division.module#DivisionPageModule'
        }]
      },

      {
        path: '',
        redirectTo: '/dashboard',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    redirectTo: '/login',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TabsPageRoutingModule { }
