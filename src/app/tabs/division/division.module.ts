import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { DivisionPage } from './division.page';
import { TextAvatarModule } from 'src/app/directives/text-avatar';

const routes: Routes = [
  {
    path: '',
    component: DivisionPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TextAvatarModule,
    RouterModule.forChild(routes)
  ],
  declarations: [DivisionPage]
})
export class DivisionPageModule { }
