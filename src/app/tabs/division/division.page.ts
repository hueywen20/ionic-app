import { Component, OnInit } from '@angular/core';
import { ReferralsService } from 'src/app/services/api/referrals.service';
import { Referrals } from '../../model/referrals';
import { LoadingController } from '@ionic/angular';


@Component({
  selector: 'app-division',
  templateUrl: './division.page.html',
  styleUrls: ['./division.page.scss'],
})
export class DivisionPage implements OnInit {

  referrals: Referrals[];

  referral;

  searchText = '';

  filter: any = {
    // branch: null,
    status: null,
    //     minDate: null,
    //     maxDate: null,
    //     product: null,
    //     division: null,
    //     productType: null,
  };

  constructor(private referralService: ReferralsService, private loadingController: LoadingController) { }

  ngOnInit() {
    this.presentLoadingWithOptions();
    this.showAll();
  }

  filterCondition(referral) {
    return referral.name.toLowerCase().indexOf(this.searchText.toLowerCase()) !== -1;
  }


  showAll() {
    this.presentLoadingWithOptions();
    this.referralService.getAllReferrals().subscribe(
      (res: any) => {
        this.referrals = res.result;
      },
      err => console.error(err)
    );
    console.log('all');
  }

  showSuccessful() {
    this.presentLoadingWithOptions();

    this.filter.status = 'Successful';
    console.log(this.filter.status);
    this.referralService.filterReferralStatus(this.filter).subscribe(
      (res: any) => {
        console.log(res.result);
        this.referrals = res.result;
      },
      err => console.error(err)
    );
    console.log('sucessful');
  }

  showUnsuccessful() {
    this.presentLoadingWithOptions();

    this.filter.status = 'Unsuccessful';
    console.log(this.filter.status);
    this.referralService.filterReferralStatus(this.filter).subscribe(
      (res: any) => {
        console.log(res.result);
        this.referrals = res.result;
      },
      err => console.error(err)
    );
    console.log('unsuccessful');
  }

  showInProgress() {
    this.presentLoadingWithOptions();

    this.filter.status = 'In Progress';
    console.log(this.filter.status);
    this.referralService.filterReferralStatus(this.filter).subscribe(
      (res: any) => {
        console.log(res.result);
        this.referrals = res.result;
      },
      err => console.error(err)


    );

    console.log('in progress');
  }

  async presentLoadingWithOptions() {
    const loading = await this.loadingController.create({
      spinner: 'crescent',
      duration: 2000,
      message: 'Please wait...',
      translucent: true,
      cssClass: 'custom-class custom-loading'
    });
    return await loading.present();
  }

}
