import { Component, OnInit } from '@angular/core';
import { KeycloakService } from 'keycloak-angular';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {

  constructor(private keycloakAngular: KeycloakService) { }

  ngOnInit() {
  }

  logout() {
    this.keycloakAngular.logout();
  }
}
