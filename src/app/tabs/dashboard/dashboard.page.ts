import { Component, OnInit } from '@angular/core';

import * as HighCharts from 'highcharts';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.page.html',
  styleUrls: ['./dashboard.page.scss'],
})
export class DashboardPage implements OnInit {

  constructor() { }

  ionViewDidEnter() {
    // this.plotSimpleBarChart();
    this.plotSimplePieChart();
    this.plotSimplePieChart2();
  }

  plotSimpleBarChart() {
    let myChart = HighCharts.chart('highcharts', {
      chart: {
        type: 'bar'
      },
      title: {
        text: 'Fruit Consumption'
      },
      xAxis: {
        categories: ['Apples', 'Bananas', 'Oranges']
      },
      yAxis: {
        title: {
          text: 'Fruit eaten'
        }
      },
      series: [
        {
          name: 'Jane',
          type: undefined,
          data: [1, 10, 4]
        },
        {
          name: 'John',
          type: undefined,
          data: [5, 7, 3]
        }]
    });
  }

  plotSimplePieChart() {
    let myChart = HighCharts.chart('highcharts', {
      chart: {
        renderTo: 'container',
        type: 'pie'
      },
      title: {
        text: '$ 2.75 mil',
        align: 'center',
        x: 3,
        y: -6,
        verticalAlign: 'middle',
        style: {
          fontSize: '20',
          margin: 'auto'
        }
      },
      plotOptions: {
        pie: {
            showInLegend: true,
            dataLabels: {
              enabled: false
            },
            innerSize: '60%',
            colors: [
              '#22bb33', '#ffae42', '#bb2124'
            ]
        }
      },
      tooltip: {
        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
    },
      series: [{
        type: 'pie',
        name: 'FUM of Referral ($)',
        innerSize: '70%',
        data: [
            ['Successful', 1004177],
            ['In Progress', 1671401],
            ['Unsuccessful', 70000]
        ]
    }]
    });
  }

  plotSimplePieChart2() {
    let myChart = HighCharts.chart('highcharts2', {
      chart: {
        renderTo: 'container',
        type: 'pie'
      },
      title: {
        text: '68',
        align: 'center',
        x: 0,
        y: -10,
        verticalAlign: 'middle',
        style: {
          fontSize: '20',
          margin: 'auto'
        }
      },
      plotOptions: {
        pie: {
            showInLegend: true,
            dataLabels: {
              enabled: false
            },
            innerSize: '60%',
            colors: [
              '#22bb33', '#ffae42', '#bb2124'
            ]
        }
      },
      tooltip: {
        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
    },
      series: [{
        type: 'pie',
        name: 'Number of Referral',
        innerSize: '70%',
        data: [
            ['Successful', 5],
            ['In Progress', 60],
            ['Unsuccessful', 3]
        ]
    }]
    });
  }


  ngOnInit() {}

}
