import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ReferralsPage } from './referrals.page';
import { TextAvatarModule } from '../../directives/text-avatar';


const routes: Routes = [
  {
    path: '',
    component: ReferralsPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TextAvatarModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ReferralsPage]
})
export class ReferralsPageModule { }
