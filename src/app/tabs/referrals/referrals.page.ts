import { Component, OnInit } from '@angular/core';
import { Referrals } from '../../model/referrals';
import { ReferralsService } from 'src/app/services/api/referrals.service';
import { ActivatedRoute } from '@angular/router';

import * as HighCharts from 'highcharts';
import { LoadingController } from '@ionic/angular';
import { ShowAllReferralPage } from '../../referrals/show-all-referral/show-all-referral.page';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-referrals',
  templateUrl: './referrals.page.html',
  styleUrls: ['./referrals.page.scss'],
})
export class ReferralsPage implements OnInit {

  referrals: Referrals[];

  constructor(private referralService: ReferralsService, private activatedRoute: ActivatedRoute,
    public loadingController: LoadingController, public modalController: ModalController) { }

  ngOnInit() {

    this.getLatestReferralActivities();
    //   const params = this.activatedRoute.snapshot.params;

    //   if (params.id) {
    //     this.referralService.getLatestReferralActivities(params.id).subscribe(
    //       (res: any) => {
    //         console.log('res.result: ', res.result);
    //         this.referrals = res.result;
    //         console.log('this.referrals: ', this.referrals);
    //       },
    //       err => console.error(err)
    //     );
    //   }
  }

  // ngOnInit() {
  //   this.getAllReferrals();

  //   const params = this.activatedRoute.snapshot.params;
  //   if (params.id) {
  //     this.referralService.getReferral(params.id).subscribe(
  //       (res: any) => {
  //         this.referrals = res.result;
  //       },
  //       err => console.error(err)
  //     );
  //   }
  // }

  ionViewDidEnter() {
    this.plotSimplePieChart3();
    this.getLatestReferralActivities();
  }

  plotSimplePieChart3() {
    const myChart = HighCharts.chart('highcharts3', {
      chart: {
        renderTo: 'container',
        type: 'pie'
      },
      title: {
        text: '68',
        align: 'center',
        x: 0,
        y: -10,
        verticalAlign: 'middle',
        style: {
          fontSize: '20',
          margin: 'auto'
        }
      },
      plotOptions: {
        pie: {
          showInLegend: true,
          dataLabels: {
            enabled: false
          },
          innerSize: '60%',
          colors: [
            '#22bb33', '#ffae42', '#bb2124'
          ]
        }
      },
      tooltip: {
        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
      },
      series: [{
        type: 'pie',
        name: 'Number of Referral',
        innerSize: '70%',
        data: [
          ['Successful', 5],
          ['In Progress', 60],
          ['Unsuccessful', 3]
        ]
      }]
    });
  }


  getAllReferrals() {
    this.referralService.getAllReferrals().subscribe(
      // (res: Customers[]) => {
      //   this.customers = res;
      // },
      (res: any) => {
        this.referrals = res.result;
      },
      err => console.error(err)
    );
  }

  getLatestReferralActivities() {
    // const params = this.activatedRoute.snapshot.params;

    // this.referralService.getLatestReferralActivities(params.id).subscribe(
    this.referralService.getLatestReferralActivities('5dad68336c98d72efa0093c9').subscribe(
      (res: any) => {
        this.referrals = res.result;

        console.log('getting latest 3', res.result);
      },
      err => console.error(err)
    );
  }

  async presentLoadingWithOptions() {
    const loading = await this.loadingController.create({
      spinner: 'crescent',
      duration: 3000,
      message: 'Please wait...',
      translucent: true,
      cssClass: 'custom-class custom-loading'
    });
    return await loading.present();
  }

  async openModal() {
    const modal = await this.modalController.create({
      component: ShowAllReferralPage
    });
    return await modal.present();
  }

}

