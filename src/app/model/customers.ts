export interface Customers {

    _id?: string;
    firstName: string;
    lastName: string;
    email: string;
    phone: number;
    address: string;
    createdAt: Date;
}
