export interface Referrals {

    _id?: string;
    name: string;
    id: string;
    email: string;
    phone: string;
    address: string;
    description: string;
    referrer: string;
    status: string;
    FUM: number;
    createdAt: Date;
}
