import { KeycloakService } from 'keycloak-angular';

export function KeycloakInit(keycloak: KeycloakService): () => Promise<any> {
    return (): Promise<any> => {
        return new Promise(async (resolve, reject) => {
            try {
                await keycloak.init({
                    config: {
                        url: 'https://10.100.9.15:8445/auth',
                        realm: 'Cambodia',
                        clientId: 'ReferralApp'
                    },
                    initOptions: {
                        onLoad: 'login-required',
                        checkLoginIframe: false
                    },
                    bearerExcludedUrls: [],
                });
                resolve();
            } catch (error) {
                reject(error);
            }
        });
    };
}
