import { TestBed } from '@angular/core/testing';

import { ReferralsService } from './referrals.service';

describe('ReferralsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ReferralsService = TestBed.get(ReferralsService);
    expect(service).toBeTruthy();
  });
});
