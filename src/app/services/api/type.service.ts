import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class TypeService {

  // API = 'https://warm-springs-44843.herokuapp.com/api';

  API = 'http://10.43.2.41:3000/api';

  constructor(private http: HttpClient) { }

  getAllTypes() {
    return this.http.get(`${this.API}/product-type`);
  }
}
