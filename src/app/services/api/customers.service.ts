import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Customers } from '../../model/customers';

@Injectable({
  providedIn: 'root'
})
export class CustomersService {

  // localAPI = 'https://warm-springs-44843.herokuapp.com/api';

  API = 'http://10.43.2.41:3000/api';

  constructor(private http: HttpClient) { }

  getAllCustomer() {
    return this.http.get(`${this.API}/customer`);
  }

  getCustomer(id: string) {
    return this.http.get(`${this.API}/customer/${id}`);
  }

  saveCustomer(customer: Customers) {
    return this.http.post(`${this.API}/customer/create`, customer);
  }

  updateCustomer(id: string | number, updateCustomer: Customers) {
    return this.http.put(`${this.API}/customer/update?customerID=${id}`, updateCustomer);
  }

  deleteCustomer(id: string) {
    return this.http.delete(`${this.API}/customer/delete?customerID=${id}`);
  }
}

