import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Referrals } from '../../model/referrals';

@Injectable({
  providedIn: 'root'
})
export class ReferralsService {

  // API = 'https://warm-springs-44843.herokuapp.com/api';

  API = 'http://10.43.2.41:3000/api';

  constructor(private http: HttpClient) { }

  // get referral
  getAllReferrals() {
    return this.http.get(`${this.API}/referral`);
  }

  // get specific referral with id
  getReferral(id: string) {
    return this.http.get(`${this.API}/referral/${id}`);
  }

  // get the latest 3
  getLatestReferralActivities(id: string) {
    return this.http.get(`${this.API}/referral/get-latest-ra-by-user/${id}`);
  }

  // get referall based on status
  filterReferralStatus(filter: any) {
    return this.http.put(`${this.API}/referral/get-filter-referral`, filter);
  }

  // create referral
  addReferral(referral: Referrals) {
    return this.http.post(`${this.API}/referral`, referral);
  }

  // update referral
  updateReferral(id: string | number, updateReferral: Referrals) {
    return this.http.put(`${this.API}/referral/update?referralID=${id}`, updateReferral);
  }


  // delete referral
  deleteReferral(id: string) {
    return this.http.delete(`${this.API}/referral/delete?referralID=${id}`);
  }
}

