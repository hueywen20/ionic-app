import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { BranchService } from 'src/app/services/api/branch.service';
import { DivisionService } from 'src/app/services/api/division.service';
import { TypeService } from 'src/app/services/api/type.service';
import { ProductService } from 'src/app/services/api/product.service';

@Component({
  selector: 'app-add-referral',
  templateUrl: './add-referral.page.html',
  styleUrls: ['./add-referral.page.scss'],
})
export class AddReferralPage implements OnInit {

  userForm: FormGroup;

  branches: any;
  divisions: any;
  types: any;
  products: any;
  status: any;

  constructor(private formBuilder: FormBuilder, private branchService: BranchService, private divisionService: DivisionService,
    private typeService: TypeService, private productService: ProductService) { }

  ngOnInit() {
    this.userForm = this.formBuilder.group({
      name: ['', Validators.required],
      contact: ['', Validators.required],
      email: [''],
      status: ['In Progress'],
      amount: ['', Validators.required]
    });

    this.getBranches();
    this.getDivisions();
    this.getProducts();
    this.getTypes();
  }

  getBranches() {
    this.branchService.getAllBranches().subscribe(
      (res: any) => {
        this.branches = res.result;
      },
      err => console.error(err)
    );
  }

  getDivisions() {
    this.divisionService.getAllDivisions().subscribe(
      (res: any) => {
        this.divisions = res.result;
      },
      err => console.error(err)
    );
  }

  getTypes() {
    this.typeService.getAllTypes().subscribe(
      (res: any) => {
        this.types = res.result;
      },
      err => console.error(err)
    );
  }

  getProducts() {
    this.productService.getAllProducts().subscribe(
      (res: any) => {
        this.products = res.result;
      },
      err => console.error(err)
    );
  }

  // reset() {
  //   this.branches = '';
  //   this.divisions = '';
  //   this.types = '';
  //   this.products = '';
  //   this.status = 'In Progress';
  // }

}
