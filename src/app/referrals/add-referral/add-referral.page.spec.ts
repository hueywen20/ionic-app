import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddReferralPage } from './add-referral.page';

describe('AddReferralPage', () => {
  let component: AddReferralPage;
  let fixture: ComponentFixture<AddReferralPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddReferralPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddReferralPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
