import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewReferralPage } from './view-referral.page';

describe('ViewReferralPage', () => {
  let component: ViewReferralPage;
  let fixture: ComponentFixture<ViewReferralPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewReferralPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewReferralPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
