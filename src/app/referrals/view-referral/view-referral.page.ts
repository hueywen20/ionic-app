import { Component, OnInit } from '@angular/core';
import { ReferralsService } from 'src/app/services/api/referrals.service';
import { ActivatedRoute } from '@angular/router';
import { Referrals } from '../../model/referrals';

@Component({
  selector: 'app-view-referral',
  templateUrl: './view-referral.page.html',
  styleUrls: ['./view-referral.page.scss'],
})
export class ViewReferralPage implements OnInit {

  referral: Referrals = {
    _id: '',
    id: '',
    name: '',
    email: '',
    phone: '',
    address: '',
    description: '',
    referrer: '',
    status: '',
    FUM: 0,
    createdAt: new Date()
  };

  constructor(private referralService: ReferralsService, private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    const params = this.activatedRoute.snapshot.params;

    if (params.id) {
      console.log(params.id);
      this.referralService.getReferral(params.id).subscribe(
        (res: any) => {
          console.log('res.result: ', res.result);
          this.referral = res.result;
          console.log('this.referrals:  ', this.referral);

        },
        err => console.error(err)
      );
    }

  }

}
