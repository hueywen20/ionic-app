import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ViewReferralPage } from './view-referral.page';

import { TextAvatarModule } from '../../directives/text-avatar';


const routes: Routes = [
  {
    path: '',
    component: ViewReferralPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TextAvatarModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ViewReferralPage]
})
export class ViewReferralPageModule { }
