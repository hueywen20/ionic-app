import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ShowAllReferralPage } from './show-all-referral.page';

import { TextAvatarModule } from 'src/app/directives/text-avatar';

const routes: Routes = [
  {
    path: '',
    component: ShowAllReferralPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TextAvatarModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ShowAllReferralPage]
})
export class ShowAllReferralPageModule { }
