import { Component, OnInit } from '@angular/core';
import { Referrals } from '../../model/referrals';
import { ReferralsService } from 'src/app/services/api/referrals.service';
import { LoadingController, ModalController } from '@ionic/angular';

@Component({
  selector: 'app-show-all-referral',
  templateUrl: './show-all-referral.page.html',
  styleUrls: ['./show-all-referral.page.scss'],
})
export class ShowAllReferralPage implements OnInit {

  referrals: Referrals[];

  referral;

  searchText = '';

  constructor(private referralService: ReferralsService, public loadingController: LoadingController,
    public modalController: ModalController) { }

  ngOnInit() {
    this.getReferrals();
  }

  filterCondition(referral) {
    return referral.name.toLowerCase().indexOf(this.searchText.toLowerCase()) !== -1;
  }

  getReferrals() {
    this.referralService.getAllReferrals().subscribe(
      (res: any) => {
        this.referrals = res.result;
      },
      err => console.error(err)
    );
  }

  async presentLoadingWithOptions() {
    const loading = await this.loadingController.create({
      spinner: 'crescent',
      duration: 2000,
      message: 'Please wait...',
      translucent: true,
      cssClass: 'custom-class custom-loading'
    });
    return await loading.present();
  }


  dismissModal() {
    this.modalController.dismiss();
  }

}
