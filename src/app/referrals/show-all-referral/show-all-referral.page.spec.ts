import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowAllReferralPage } from './show-all-referral.page';

describe('ShowAllReferralPage', () => {
  let component: ShowAllReferralPage;
  let fixture: ComponentFixture<ShowAllReferralPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShowAllReferralPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowAllReferralPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
