import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: '', loadChildren: './tabs/tabs.module#TabsPageModule' },
  { path: 'login', loadChildren: './auth/login/login.module#LoginPageModule' },
  { path: 'tutorial', loadChildren: './tutorial/tutorial.module#TutorialPageModule' },
  { path: 'view-referral/:id', loadChildren: './referrals/view-referral/view-referral.module#ViewReferralPageModule' },
  { path: 'show-all-referral', loadChildren: './referrals/show-all-referral/show-all-referral.module#ShowAllReferralPageModule' },
  { path: 'add-referral', loadChildren: './referrals/add-referral/add-referral.module#AddReferralPageModule' },
  { path: 'division', loadChildren: './tabs/division/division.module#DivisionPageModule' },
  { path: 'filter', loadChildren: './shared/filter/filter.module#FilterPageModule' },
  // { path: 'dashboard', loadChildren: './tabs/dashboard/dashboard.module#DashboardPageModule' },
  // { path: 'referrals', loadChildren: './tabs/referrals/referrals.module#ReferralsPageModule' },
  // { path: 'profile', loadChildren: './tabs/profile/profile.module#ProfilePageModule' }
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
