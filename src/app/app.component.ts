import { Component, OnInit } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { KeycloakService } from 'keycloak-angular';
import { AppState } from './services/access/app.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent implements OnInit {

  keycloakRole: string;

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    public appState: AppState,
    private keycloakAngular: KeycloakService,
    private router: Router
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  ngOnInit() {
    console.log('Initial App State', this.appState.state);
    this.keycloakRedirect();
  }

  keycloakRedirect() {
    console.log('this.keycloakAngular', this.keycloakAngular);
    if (this.keycloakAngular.isUserInRole('divisionhead')) {
      this.keycloakRole = 'divisionhead';
    } else if (this.keycloakAngular.isUserInRole('referrer')) {
      this.keycloakRole = 'referrer';
    }

    console.log(this.keycloakRole);

    if (typeof this.keycloakRole !== 'undefined' && this.keycloakRole) {
      this.router.navigateByUrl('/tabs/dashboard');
    } else {
      alert('Sorry, your role is not available. Please contact our admin. Thank you.');
      this.keycloakAngular.logout();
      console.log('Your credential not valid');
    }
  }

}
